// Name: Sittisak Choolak
// Student ID: 6450110013
// PSU, Trang Campus

void main(){
  // ---------------------------------------------------------------- //
  print("#1 Variables");
  // ---------------------------------------------------------------- //
  double temperature = 20;
  int value = 2;
  String pizza = 'pizza';
  String pasta = 'pasta';

  print("The temperature is ${temperature.toInt()}C");
  print("$value plus $value makes ${value += value}");
  print("I like $pizza and $pasta");
}