// Name: Sittisak Choolak
// Student ID: 6450110013

import 'dart:io';

void main() {
  // ---------------------------------------------------------------- //
  print("#4 List – Sum number");
  // ---------------------------------------------------------------- //
  var values = [0, 0, 0, 0, 0];
  var sum = 0, count = 0;

  for (var value=0; value<values.length; value++) {
    print("Enter value [${count+=1}]...");
    values[value] = int.parse(stdin.readLineSync()!);
    sum += values[value];
  }
  print("Sum of $values is $sum");
}